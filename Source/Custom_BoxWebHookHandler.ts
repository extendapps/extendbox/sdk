/**
 * @copyright 2019 Extend Apps Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * @NScriptType plugintypeimpl
 */

import {WebHookEvent} from '@extendapps/boxtypings/Objects/WebHookEvent';
import * as log from 'N/log';
import * as file from 'N/file';
import {WebHookEventFunction} from '@extendapps/boxtypings/Interfaces/IWebHookEvent';
import {BOX_API} from '@extendapps/boxtypings/BOX_API';

// noinspection JSUnusedGlobalSymbols
export let processWebHookEvent: WebHookEventFunction = (webHookEvent: WebHookEvent, box: BOX_API) => {
    box.downloadFile({
        file_id: webHookEvent.source.id,
        OK: details => {
            log.audit('downloadFile', details);
            let newFile: file.File = file.create({
                name: webHookEvent.source.name,
                description: webHookEvent.source.description,
                folder: -15,
                fileType: file.Type.XMLDOC,
                contents: details.contents
            });

            log.audit(`Saving ${webHookEvent.source.name}`, newFile.save());

        }, fail: clientResponse => {
            log.error('downloadFile', clientResponse);
        }
    });
};