# Extend Apps - BOX.com - SDK
***
## Installation Notes
1. Install Extend Apps' License Manager (266369)
2. Install Extend Apps' BOX Bundle (287321)
3. Create an `Custom App` in Box.com
    * Navigate to https://app.box.com/developers/services/edit/
    * Choose 'Custom App'
    * Choose 'Standard OAuth 2.0' (User Authentication)
    * Name: _**NAME_NetSuite**_ (example: _ExtendApps_NetSuite_)
    * Capture the following information from the new Custom App
        * **Client ID**
        * **Client Secret**
3. Create a new BOX Configuration record in NetSuite
    * Name: (ex: RAMP)
    * Client ID: copy in from previous step
    * Client Secret: copy in from previous step
    * Save
        * _Notice the Redirect URL field!_
4. Navigate back to the new BOX `Custom App`
    * Update the `OAuth 2.0 Redirect URI` with the Redirect URL generated from NetSuite
    * Ensure `Manage webhooks` is checked (under Application Scopes)
    * Save Changes
5. Back on the NetSuite BOX Configuration record
    * Click 'Authorize' button on the newly saved BOX Configuration record
        * You'll be taken to a BOX.com login screen asking permission to grant access
    * Click 'Grant Access to Box'
    * You should be redirected back to the BOX Configuration record
        * Notice the Code and Refresh Token fields are now populated
 
You now have a private/secure two-way communication with BOX!
***
## Registering WebHooks
Webhooks provide a way for BOX.com to notify NetSuite, in realtime, of events happening in BOX.com.  For example, you could register to be notified when a file is uploaded to a particular folder.
 
### Example: Listen for news files upload to folder /ABC
1. In BOX.com, navigate to the folder /ABC
    * Looking at the URL, take note of the ID (the numbers at the end of the URL) [Example: https://ramp.app.box.com/folder/79215787328]
2. In NetSuite, open the BOX Configuration record
3. Under the WebHook subtab, click  `New BOX Webhook`
4. Setup the WebHook settings
    * Triggers: FILE.UPLOADED
    * Target Type: folder
    * Target ID: 79215787328
    * Click `Save`
5. The screen will refresh and the `IsConnected` field will validate if evrything registered ok.

Now, anytime a file is uploaded to folder /ABC, NetSuite will be notified and trigger the WebHook Handler

    

    